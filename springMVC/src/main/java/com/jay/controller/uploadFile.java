package com.jay.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

@Controller
public class uploadFile {

    @RequestMapping("/upfile.do")
    public String upfile(Model model, HttpServletRequest request, MultipartFile picture)
            throws IOException {
        System.out.println("访问后台。。。");
    //   String pictureName=request.getParameter("picture");
        request.setAttribute("picture",picture.getOriginalFilename());
        String picName= UUID.randomUUID().toString();
        /*获取原始图片名称*/
        String oriName=picture.getOriginalFilename();
        /*新的图片名称(uuid随机数加上后缀名)*/
        String extName=oriName.substring(oriName.lastIndexOf("."));
        /*将内存图片写入磁盘*/
        picture.transferTo(new File("C:/upfile1/"+picName+extName));
        System.out.println(request.getParameter("picture"));
        return "forward:/uploadPictureJsp.jsp";
    }
}


