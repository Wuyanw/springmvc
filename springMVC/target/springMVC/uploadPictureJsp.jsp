<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>上传图片</title>
</head>
<body>
<form action="/upfile.do" method="post" enctype="multipart/form-data">
    <c:if test="${picture!=null}">
    <img src="/upfile/${picture}" width="100px" height="100px">
    </c:if>
    <input type="file" name="picture">
    <input type="submit">
</body>
</html>
